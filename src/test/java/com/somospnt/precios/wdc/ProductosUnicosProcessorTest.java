package com.somospnt.precios.wdc;

import com.somospnt.precios.PreciosAbstractTest;
import com.somospnt.precios.domain.Producto;
import java.util.Collection;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

public class ProductosUnicosProcessorTest extends PreciosAbstractTest {

    @Autowired
    private ProductosProcessor productosUnicosProcessor;

    @Test
    public void process_checkProducto_estaNormalizado() {
        String origen = "WALMART";
        String descWalmart = "Dulce de Leche La Serenisima Colonial pote 1Kg";
        String descNormalizada = "Dulce de Leche La Serenisima Colonial 1kg";
        
        List<Producto> productos = obtenerProductosVigentes(origen);
        
        Producto productoAntes = obtenerProducto(productos, descWalmart);
        Assert.assertNotNull(productoAntes);
        Assert.assertNotEquals(descNormalizada, productoAntes.getDescripcion());

        productosUnicosProcessor.process(obtenerProductosVigentes(origen), null);
        
        Producto productoDespues = obtenerProducto(productosUnicosProcessor.getProductos(), descNormalizada);
        Assert.assertEquals(descNormalizada, productoDespues.getDescripcion());
    }
    
    private List<Producto> obtenerProductosVigentes(String origen) throws DataAccessException {
        String sql = "select * from producto where origen = ? and vigente = true";
        return jdbcTemplate.query(sql,
                new Object[]{origen},
                new BeanPropertyRowMapper(Producto.class) {
                });
    }
    
    private Producto obtenerProducto(Collection<Producto> productos, String descripcion) {
        Producto productoEncontrado = null;
        for (Producto producto : productos) {
            if (descripcion.equals(producto.getDescripcion())) {
                productoEncontrado = producto;
                break;
            }
        }
        return productoEncontrado;
    }
    
}

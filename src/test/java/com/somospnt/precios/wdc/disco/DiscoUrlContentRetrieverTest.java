/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.disco;

import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class DiscoUrlContentRetrieverTest {

    private DiscoUrlContentRetriever contentRetriever;
    private DiscoUrlIterator urlIterator;

    @Before
    public void setup() {
        contentRetriever = new DiscoUrlContentRetriever();
        urlIterator = new DiscoUrlIterator();
        urlIterator.init();
    }

    @Test
    public void getContent_hayContenido_devuelveContenido() throws IOException {
        String contenido = contentRetriever.getContent(urlIterator.next());
        assertFalse(contenido.isEmpty());
        assertTrue(contenido.length() > 1000);
    }

}

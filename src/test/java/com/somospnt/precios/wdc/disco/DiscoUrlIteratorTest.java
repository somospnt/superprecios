/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.disco;

import com.altogamer.wdc.Url;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class DiscoUrlIteratorTest {

    private DiscoUrlIterator urlIterator;

    @Before
    public void setup() {
        urlIterator = new DiscoUrlIterator();
        urlIterator.init();
    }

    @Test
    public void next_hayMenues_devuelveUrlValidas() {
        int cantidad = 0;
        for (int i = 0; i < 600; i++) {
            cantidad++;
            Url url = urlIterator.next();
            if (url == null) break;
            System.out.println(url);
        }
        assertTrue("Debe haber al menos 500 urls", cantidad > 500);
    }

}

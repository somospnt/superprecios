/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.disco;

import com.altogamer.wdc.Collector;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.UrlIterator;
import com.somospnt.precios.wdc.AbstractCrawlerTest;

public class DiscoCrawlerTest extends AbstractCrawlerTest {

    @Override
    protected UrlIterator getUrlIterator() {
        DiscoUrlIterator iterator = new DiscoUrlIterator();
        iterator.setLimit(3);
        return iterator;
    }

    @Override
    protected Collector getCollector() {
        return new DiscoCollector();
    }

    @Override
    protected ContentRetriever getUrlContentRetriever() {
        return new DiscoUrlContentRetriever();
    }

    @Override
    protected String getNombreDescriptivo() {
        return "Disco";
    }

}

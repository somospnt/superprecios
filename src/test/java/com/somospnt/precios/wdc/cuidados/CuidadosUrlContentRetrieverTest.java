/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.cuidados;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.impl.SimpleUrlContentRetriever;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CuidadosUrlContentRetrieverTest {

    private SimpleUrlContentRetriever contentRetriever;
    private CuidadosUrlIterator urlIterator;

    @Before
    public void setup() {
        contentRetriever = new SimpleUrlContentRetriever();
        urlIterator = new CuidadosUrlIterator();
        urlIterator.init();
    }

    @Test
    public void getContent_hayContenido_devuelveContenido() throws IOException {
        Url url = urlIterator.next();
        String contenido = contentRetriever.getContent(url);
        System.out.println(contenido);
        assertFalse(contenido.isEmpty());
        assertTrue(contenido.length() > 1000);
    }

}

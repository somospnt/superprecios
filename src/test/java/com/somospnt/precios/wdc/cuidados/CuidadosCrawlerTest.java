/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.cuidados;

import com.altogamer.wdc.Collector;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.UrlIterator;
import com.altogamer.wdc.impl.SimpleUrlContentRetriever;
import com.somospnt.precios.wdc.AbstractCrawlerTest;

public class CuidadosCrawlerTest extends AbstractCrawlerTest {

    @Override
    protected UrlIterator getUrlIterator() {
        CuidadosUrlIterator iterator = new CuidadosUrlIterator();
        return iterator;
    }

    @Override
    protected Collector getCollector() {
        return new CuidadosCollector();
    }

    @Override
    protected ContentRetriever getUrlContentRetriever() {
        return new SimpleUrlContentRetriever();
    }

    @Override
    protected String getNombreDescriptivo() {
        return "Precios Cuidados";
    }

}

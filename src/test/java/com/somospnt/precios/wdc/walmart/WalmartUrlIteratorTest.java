/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.walmart;

import com.altogamer.wdc.Url;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class WalmartUrlIteratorTest {

    private WalmartUrlIterator urlIterator;

    @Before
    public void setup() {
        urlIterator = new WalmartUrlIterator();
        urlIterator.init();
    }

    @Test
    public void next_hayMenues_devuelveUrlValidas() {
        int cantidad = 0;
        for (int i = 0; i < 30; i++) {
            cantidad++;
            Url url = urlIterator.next();
            if (url == null) break;
            System.out.println(url);
        }
        assertTrue("Debe haber al menos 29 urls", cantidad > 29);
    }

    @Test
    public void foo() {
        while (true) {
            Url url = urlIterator.next();
            if (url == null) {
                break;
            }
            System.out.println(url);
        }
    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc;

import com.altogamer.wdc.Collector;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.UrlIterator;
import com.altogamer.wdc.WebCrawler;
import com.somospnt.precios.PreciosAbstractTest;
import com.somospnt.precios.domain.Producto;
import java.util.Collection;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractCrawlerTest extends PreciosAbstractTest {
    @Autowired
    private ProductosProcessor productosProcessor;

    protected abstract UrlIterator getUrlIterator();

    protected abstract Collector getCollector();

    protected abstract ContentRetriever getUrlContentRetriever();

    protected abstract String getNombreDescriptivo();

    protected int getCantidadMinimaDeResultadosEsperada() {
        return 10;
    }

    protected int getCorePoolSize() {
        return 5;
    }

    protected int getMaximunPoolSize() {
        return 5;
    }

    protected void validarProducto(Producto producto) {
        assertFalse("La descripcion es obligatoria", producto.getDescripcion().trim().isEmpty());
    }

    @Test
    public void crawl() throws Exception {
        System.out.println("Iniciando verificacion de productos de " + getNombreDescriptivo());
        productosProcessor.clear();

        UrlIterator urlIterator = getUrlIterator();

        WebCrawler crawler = new WebCrawler();
        crawler.setCollector(getCollector());
        crawler.setProcessor(productosProcessor);
        crawler.setUrlIterator(urlIterator);
        crawler.setContentRetriever(getUrlContentRetriever());

        crawler.setCorePoolSize(getCorePoolSize());
        crawler.setMaximumPoolSize(getMaximunPoolSize());

        crawler.run();

        Collection<Producto> productos = productosProcessor.getProductos();
        assertTrue("Tienen que encontrarse al menos " + getCantidadMinimaDeResultadosEsperada() + " productos", productos.size() > getCantidadMinimaDeResultadosEsperada());
        productos.stream().forEach(producto -> {
            System.out.println(producto);
            validarProducto(producto);
        });
        System.out.println(productos.size() + " productos verificados de " + getNombreDescriptivo());
    }

}

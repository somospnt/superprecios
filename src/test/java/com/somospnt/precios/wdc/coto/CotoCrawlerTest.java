/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.coto;

import com.altogamer.wdc.Collector;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.UrlIterator;
import com.somospnt.precios.wdc.AbstractCrawlerTest;

public class CotoCrawlerTest extends AbstractCrawlerTest {

    @Override
    protected UrlIterator getUrlIterator() {
        CotoUrlIterator iterator = new CotoUrlIterator();
        iterator.setLimit(2);
        return iterator;
    }

    @Override
    protected Collector getCollector() {
        return new CotoCollector();
    }

    @Override
    protected ContentRetriever getUrlContentRetriever() {
        return new CotoUrlContentRetriever();
    }

    @Override
    protected String getNombreDescriptivo() {
        return "Coto";
    }

}

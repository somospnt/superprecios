/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.coto;

import com.altogamer.wdc.Url;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class CotoUrlIteratorTest {

    private CotoUrlIterator urlIterator;

    @Before
    public void setup() {
        urlIterator = new CotoUrlIterator();
        urlIterator.init();
    }

    @Test
    public void next_primeraInvocacion_devuelveUrl() throws IOException {
        Url url = urlIterator.next();
        assertNotNull(url);
        assertTrue(url.getUrl().length() > 10);
    }

//    @Test
    public void imprimirTodasLasUrl() {
        Url url;
        int i;
        for (i=0; (url = urlIterator.next()) != null; i++) {
            System.out.println(url);
        }
        System.out.printf("%d urls encontradas\n", i);
    }

}

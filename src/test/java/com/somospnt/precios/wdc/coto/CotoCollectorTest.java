/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.coto;

import com.altogamer.wdc.Url;
import com.somospnt.precios.domain.Producto;
import java.io.IOException;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotoCollectorTest {

    private CotoCollector collector;
    private CotoUrlContentRetriever contentRetriever;

    @Before
    public void setup() {
        contentRetriever = new CotoUrlContentRetriever();
        collector = new CotoCollector();
    }

    private String getContenidoValido() throws IOException {
        Url url = new Url(Url.Method.GET, "http://www.cotodigital.com.ar/l.asp?cat=966&id=966");
        return contentRetriever.getContent(url);
    }

    private String getContenidoVacio() throws IOException {
        Url url = new Url(Url.Method.GET, "http://www.cotodigital.com.ar/l.asp?cat=966&id=966&start=61");
        return contentRetriever.getContent(url);
    }

    @Test
    public void collect_hayContenido_devuelveProductos() throws IOException {
        String html = getContenidoValido();

        List<Producto> productos = collector.collect(html);
        productos.stream().forEach(System.out::println);
        assertTrue(productos.size() > 10);
    }

    @Test
    public void collect_noHayContenido_devuelveProductosVacios() throws IOException {
        String html = getContenidoVacio();
        List<Producto> productos = collector.collect(html);
        assertTrue(productos.isEmpty());
    }

}

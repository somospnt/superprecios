/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.coto;

import com.altogamer.wdc.Url;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotoUrlContentRetrieverTest {

    private CotoUrlContentRetriever contentRetriever;

    @Before
    public void setup() {
        contentRetriever = new CotoUrlContentRetriever();
    }

    @Test
    public void getContent_hayContenido_devuelveContenido() throws IOException {
        Url url = new Url(Url.Method.GET, "http://www.cotodigital.com.ar/l.asp?cat=1291&id=1291");
        String contenido = contentRetriever.getContent(url);
        System.out.println(contenido);
        assertFalse(contenido.isEmpty());
        assertTrue(contenido.length() > 1000);
    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.service;

import com.somospnt.precios.PreciosAbstractTest;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

public class ProductoServiceTest extends PreciosAbstractTest {

    @Autowired
    private ProductoService productoService;

    @Test
    public void buscarHistorico_hayResultados_devuelveProductos() {
        Long idProducto = 1L;
        List<Producto> productos = productoService.buscarHistorico(idProducto);
        productos.stream().forEach(p -> {
            assertEquals("Dulce De Leche Jumbo", p.getDescripcion());
            assertTrue(p.getPrecioJumbo().doubleValue() > 0);
        });
    }

    @Test
    public void buscarHistorico_noHayResultados_devuelveListaVacia() {
        Long idProducto = Long.MIN_VALUE;
        List<Producto> productos = productoService.buscarHistorico(idProducto);
        assertTrue(productos.isEmpty());
    }

    @Test
    public void aplicarNormalizacion_conDescripcionExistente_normaliza() {
        String nuevaDescripcion = "Aceite natura x litro";
        List<String> descripcionesOrigen = new ArrayList<>();
        descripcionesOrigen.add("Aceite natura 1lt");
        descripcionesOrigen.add("Aceite natura l000cc");

        productoService.aplicarNormalizacion(nuevaDescripcion, descripcionesOrigen);

        Producto producto = jdbcTemplate.queryForObject("select * from producto where descripcion = ? and vigente = ?", new Object[]{nuevaDescripcion, true},
                new RowMapper<Producto>() {
            public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
                Producto producto = new Producto();
                producto.setDescripcion(rs.getString("descripcion"));
                producto.setPrecioJumbo(rs.getBigDecimal("precio_jumbo"));
                producto.setPrecioWalmart(rs.getBigDecimal("precio_walmart"));
                producto.setPrecioCoto(rs.getBigDecimal("precio_coto"));
                return producto;
            }
        });
        assertTrue(producto != null);
        assertNotNull(producto.getPrecio(Origen.JUMBO));
        assertNotNull(producto.getPrecio(Origen.WALMART));
        assertNotNull(producto.getPrecio(Origen.COTO));

    }

    @Test
    public void aplicarNormalizacion_conDescripcionNueva_normaliza() {
        String nuevaDescripcion = "Dulce de leche sancor 1 kg NUEVO";
        List<String> descripcionesOrigen = new ArrayList<>();
        descripcionesOrigen.add("Dulce de leche sancor 1 kg");
        descripcionesOrigen.add("Dulce De Leche La Serenisima Colonial. Pot X1kg");

        productoService.aplicarNormalizacion(nuevaDescripcion, descripcionesOrigen);

        Producto producto = jdbcTemplate.queryForObject("select * from producto where descripcion = ? and vigente = ?", new Object[]{nuevaDescripcion, true},
                new RowMapper<Producto>() {
            public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
                Producto producto = new Producto();
                producto.setDescripcion(rs.getString("descripcion"));
                producto.setPrecioJumbo(rs.getBigDecimal("precio_jumbo"));
                producto.setPrecioWalmart(rs.getBigDecimal("precio_walmart"));
                return producto;
            }
        });
        assertTrue(producto != null);
        assertNotNull(producto.getPrecio(Origen.JUMBO));
        assertNotNull(producto.getPrecio(Origen.WALMART));

    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.service;

import com.somospnt.precios.PreciosAbstractTest;
import com.somospnt.precios.domain.Normalizacion;
import com.somospnt.precios.vo.NormalizacionVo;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.*;

public class NormalizacionServiceTest extends PreciosAbstractTest {

    @Autowired
    private NormalizacionService normalizacionService;

    @Test
    public void guardar_guardaNormalizacion() {
        Integer filasAntes = jdbcTemplate.queryForObject("select count(*) from normalizacion", Integer.class);
        NormalizacionVo normalizacionVo = new NormalizacionVo();
        List<String> descripciones = new ArrayList<>();
        normalizacionVo.setDescripcion("Nueva");
        descripciones.add("Vieja");
        normalizacionVo.setDescripciones(descripciones);

        normalizacionService.guardar(normalizacionVo);

        Integer filasDespues = jdbcTemplate.queryForObject("select count(*) from normalizacion", Integer.class);
        assertTrue(filasAntes+1 == filasDespues);
    }

    @Test
    public void guardar_normalizacionExistente_guardaNormalizacion() {
        Integer filasAntes = jdbcTemplate.queryForObject("select count(*) from normalizacion", Integer.class);
        NormalizacionVo normalizacionVo = new NormalizacionVo();
        List<String> descripciones = new ArrayList<>();
        normalizacionVo.setDescripcion("descripcion nueva");
        descripciones.add("descripcion original A");
        normalizacionVo.setDescripciones(descripciones);
        normalizacionService.guardar(normalizacionVo);
        Integer filasDespues = jdbcTemplate.queryForObject("select count(*) from normalizacion", Integer.class);
        assertTrue(filasAntes+1 == filasDespues);
    }

    @Test(expected = RuntimeException.class)
    public void guardar_normalizacionExistente_lanzaRuntimeException() {
        NormalizacionVo normalizacionVo = new NormalizacionVo();
        List<String> descripciones = new ArrayList<>();
        normalizacionVo.setDescripcion("descripcion super nueva");
        descripciones.add("descripcion nueva");
        normalizacionVo.setDescripciones(descripciones);
        normalizacionService.guardar(normalizacionVo);
    }

    @Test
    public void guardar_normalizacionUnaExistente_guardaNormalizacion() {
        Integer filasAntes = jdbcTemplate.queryForObject("select count(*) from normalizacion", Integer.class);
        NormalizacionVo normalizacionVo = new NormalizacionVo();
        List<String> descripciones = new ArrayList<>();
        normalizacionVo.setDescripcion("descripcion nueva");
        descripciones.add("descripcion nueva");
        descripciones.add("descripcion original B");
        normalizacionVo.setDescripciones(descripciones);
        normalizacionService.guardar(normalizacionVo);
        Integer filasDespues = jdbcTemplate.queryForObject("select count(*) from normalizacion", Integer.class);
        assertTrue(filasAntes+1 == filasDespues);
    }

    @Test
    public void buscarTodas_retornaListaNormalizaciones() {
        List<Normalizacion> normalizaciones = normalizacionService.buscarTodas();
        assertNotNull(normalizaciones);
        assertFalse(normalizaciones.isEmpty());
    }



}

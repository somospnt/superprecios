/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.service;

import com.somospnt.precios.PreciosAbstractTest;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductoSchedulerServiceTest extends PreciosAbstractTest {

    @Autowired
    private ProductoSchedulerService productoSchedulerService;

    @Test
    @Ignore
    public void actualizarTodos() {
        Integer cantidadInicial = jdbcTemplate.queryForObject("select count(*) from producto", Integer.class);
        productoSchedulerService.actualizarTodos();
        Integer cantidadFinal = jdbcTemplate.queryForObject("select count(*) from producto", Integer.class);
        assertTrue(cantidadFinal > cantidadInicial);
    }
}

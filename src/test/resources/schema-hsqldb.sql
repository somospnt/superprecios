DROP TABLE IF EXISTS producto;
DROP TABLE IF EXISTS normalizacion;

CREATE TABLE producto (
    id BIGINT IDENTITY PRIMARY KEY,
    descripcion VARCHAR(500) NOT NULL,
    precio_cuidados DOUBLE,
    precio_coto DOUBLE,
    precio_disco DOUBLE,
    precio_jumbo DOUBLE,
    precio_walmart DOUBLE,
    fecha TIMESTAMP NOT NULL,
    vigente BOOLEAN NOT NULL
);

CREATE TABLE normalizacion (
    id BIGINT IDENTITY PRIMARY KEY,
    descripcion_origen VARCHAR(500) NOT NULL,
    descripcion_nueva VARCHAR(500) NOT NULL
);

CREATE UNIQUE INDEX uk_normalizacion_origen_descripcion on normalizacion(descripcion_origen);


INSERT INTO producto
(id,    descripcion,                                               precio_jumbo,    precio_walmart, precio_coto,     fecha,                      vigente)
VALUES
(1,     'Dulce De Leche Jumbo',                                    '54.59',         null,           null,           '2015-01-05 00:00:00.00',   true),
(2,     'Dulce De Leche Jumbo',                                    '54.59',         null,           null,           '2015-01-04 00:00:00.00',   false),
(3,     'Dulce De Leche Jumbo',                                    '54.59',         null,           null,           '2015-01-03 00:00:00.00',   false),
(4,     'Dulce De Leche Jumbo',                                    '54.59',         null,           null,           '2015-01-02 00:00:00.00',   false),
(5,     'Dulce de leche la pataia 850 gr',                         '54.90',         null,           null,           '2015-01-05 00:00:00.00',   true),
(6,     'Dulce De Leche La Serenisima Colonial. Pot X1kg',         '45.99',         null,           null,           '2015-01-05 00:00:00.00',   true),
(7,     'Dulce de leche diet regidiet 445gr',                      null,            '43.98',        null,           '2015-01-05 00:00:00.00',   true),
(8,     'Dulce de leche sancor 1 kg',                              null,            '35.30',        null,           '2015-01-05 00:00:00.00',   true),
(9,     'Dulce de leche original sancor 454 gr',                   null,            '30.55',        null,           '2015-01-05 00:00:00.00',   true),
(10,    'Vieja',                                                   '35.30',         null,           null,           '2015-01-05 00:00:00.00',   true),
(11,    'Dulce de Leche La Serenisima Colonial pote 1Kg',          null,            '40.00',        null,           '2015-01-05 00:00:00.00',   true),
(12,    'descripcion nueva'                             ,          null,            '40.00',        null,           '2015-01-05 00:00:00.00',   true),
(13,    'Aceite natura x litro',                                   null,            null,           '40.00',        '2015-01-05 00:00:00.00',   true),
(14,    'Aceite natura 1lt',                                       '45.99',         null,           null,           '2015-01-05 00:00:00.00',   true),
(15,    'Aceite natura l000cc',                                    null,            '43.98',        null,           '2015-01-05 00:00:00.00',   true);

INSERT INTO normalizacion
(descripcion_nueva, descripcion_origen)
VALUES
('Dulce de Leche La Serenisima Colonial 1kg', 'Dulce De Leche La Serenisima Colonial. Pot X1kg'),
('Dulce de Leche La Serenisima Colonial 1kg', 'Dulce de Leche La Serenisima Colonial pote 1Kg'),
('descripcion nueva', 'descripcion original');


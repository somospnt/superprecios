prc.ui.normalizacion = (function () {

    var productosSeleccionados = [];

    function init() {
        initAgregarProductoSimilar();
        initQuitarProductoSimilar();
        initNormalizar();
    }

    function initAgregarProductoSimilar() {
        $(document).on("click", ".sp-js-agregar-producto", function () {
            var $fila = $(this);
            var producto = productoDesdeFila($fila);
            var $htmlFilaProducto;

            if (!verificarProductoContraSeleccionados(producto)) {
                alert("Ya hay un producto con precio para el mismo supermercado");
                return;
            }
            productosSeleccionados.push(producto);

            $fila.hide("fast");
            $htmlFilaProducto = $("<tr class='prc-js-quitar-producto'>" + $fila.html() + "</tr>");
            $("#lista-productos-iguales").append($htmlFilaProducto);
        });
    }

    function initQuitarProductoSimilar() {
        $(document).on("click", ".prc-js-quitar-producto", function () {
            var $fila = $(this);
            var producto = productoDesdeFila($fila);
            var i;
            for (i = 0; i < productosSeleccionados.length; i++) {
                if (productosSeleccionados[i].descripcion === producto.descripcion) {
                    break;
                }
            }
            productosSeleccionados.splice(i, 1);
            $fila.remove();
            $("#lista-productos-busqueda").prepend("<tr class='sp-js-agregar-producto'>" + $fila.html() + "</tr>");
        });
    }

    /**
     * Verifica que no existe un producto seleccionado con precio en los mismos
     * origenes que el producto por parametro.
     * @return true si el producto puede ser seleccionado, false en caso contrario.
     */
    function verificarProductoContraSeleccionados(producto) {
        var origenes = prc.service.origen.origenes;
        var i, j, origenProducto;
        for (i = 0; i < origenes.length; i++) {
            origenProducto = origenes[i].variable;
            if (producto[origenProducto]) {
                for (j = 0; j < productosSeleccionados.length; j++) {
                    if (productosSeleccionados[j][origenProducto]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    function productoDesdeFila($fila) {
        var origenes = prc.service.origen.origenes;
        var producto = {};
        var i, precio;
        producto.descripcion = $fila.find(".sp-js-descripcion").html();
        for (i = 0; i < origenes.length; i++) {
            precio = $fila.find(".sp-js-precio-" + origenes[i].constante).html();
            if (precio) {
                producto[origenes[i].variable] = precio;
            }
        }
        return producto;
    }


    function initNormalizar() {
        var $botonNormalizar = $("#prc-normalizar");

        $botonNormalizar.click(function () {
            var normalizacion = {};
            var i;
            if (productosSeleccionados.length < 2) {
                alert("Seleccione al menos 2 productos.");
                return;
            }

            normalizacion.descripcion = $( "#lista-productos-iguales > tr:first-child > td:first-child").text();
            normalizacion.descripciones = [];
            for (i = 0; i < productosSeleccionados.length; i++) {
                normalizacion.descripciones.push(productosSeleccionados[i].descripcion);
            }
            console.dir(normalizacion);

            prc.service.normalizacion.guardar(normalizacion).done(function () {
                alert("La normalización se realizo con exito.");
                window.location.reload();
            }).fail(function () {
                alert("Ocurrio un error al procesar la petición.");
            });
        });

        $("table.prc-lista-precios tbody").sortable({
            placeholder: "ui-state-highlight"
        }).disableSelection();
    }

    return {
        init: init
    };
})();


$(document).ready(function () {
    prc.ui.normalizacion.init();
});
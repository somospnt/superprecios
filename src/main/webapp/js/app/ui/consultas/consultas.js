prc.ui.consultas = (function () {

    function init() {
        initMejoresPrecios();
        initTablaConsulta();
    }

    function initTablaConsulta() {
        var $table = $('table.sp-lista-productos');
        $table.floatThead({
            useAbsolutePositioning: false,
            scrollingTop: 70
        });
    }

    function initMejoresPrecios() {
        // Recorre los tr
        $('.sp-js-producto-precios').each(function (i, precios) {
            var valores = [];
            var valorMasBajo, i;
            var $precios = $(precios);
            // Armado de array de precios
            $precios.find('.sp-js-precio').each(function (i, precio) {
                var $precio = $(precio);
                var desc = $precio.text();
                var val = parseInt(desc.replace(/[\$,]/g, ''), 10);
                if (!isNaN(val)) {
                    valores.push({valor: val, $contenedor: $precio});
                }
            });
            // Si hay más de un precio para un producto
            if (valores.length > 1) {
                valores.sort(function (a, b) {
                    return a.valor - b.valor;
                });
                valorMasBajo = valores[0].valor;
                for (i = 0; i < valores.length; i++) {
                    if (valorMasBajo === valores[i].valor) {
                        valores[i].$contenedor.addClass('sp-mejorPrecio');
                    }
                    else {
                        break;
                    }
                    valorMasBajo = valores[i].valor;
                }
            }
        });
    }

    return {
        init: init
    };
})();


$(document).ready(function () {
    prc.ui.consultas.init();
});

prc.ui.detalleNormalizacion = (function () {
    function init() {
        initIntercambiarDescripciones();
    }

    function initIntercambiarDescripciones() {
        $(document).on("click", ".sp-js-descripcion-origen", function () {
            var $this = $(this);
            var $nuevo = $this.parents(".prc-js-normalizacion").find(".sp-js-descripcion-nueva");
            var descripcionNueva, descripcionOrigen;
            descripcionOrigen = $this.data("descripcion");
            descripcionNueva = $nuevo.data("descripcion");
            $nuevo.data("descripcion", descripcionOrigen);
            $nuevo.html(descripcionOrigen);
            $this.data("descripcion", descripcionNueva);
            $this.html(descripcionNueva);
        });
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    prc.ui.detalleNormalizacion.init();
});


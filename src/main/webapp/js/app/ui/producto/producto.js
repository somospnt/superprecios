prc.ui.producto = (function () {

    var origenes = prc.service.origen.origenes;

    function init() {
        var idProducto = prc.ui.getUrlParameter("id");
        $.getJSON("api/productos/" + idProducto + "/historicos", null, function (productos) {
            var datos = [];
            var $titulo = $("#prc-js-titulo-producto");
            var i, j;
            var fecha, precio;
            if (productos.length > 0) {
                $titulo.html(productos[0].descripcion);
                //inicializar el array de datos para cada origen
                for (i = 0; i < origenes.length; i++) {
                    datos[i] = {
                        label: origenes[i].nombre,
                        data: []
                    };
                }
                //cargar los datos en el array correspondiente
                for (i = 0; i < productos.length; i++) {
                    for (j = 0; j < origenes.length; j++) {
                        fecha = productos[i].fecha;
                        precio = productos[i][origenes[j].variable];
                        datos[j].data.push([fecha, precio]);
                    }
                }
                //graficar
                $.plot("#prc-chart", datos, {
                    xaxis: {mode: "time", timeformat: "%d/%m/%Y", tickSize: [7, "day"]},
                    lines: {show: true},
                    points: {show: true},
                    grid: {hoverable: true},
                    tooltip: true,
                    tooltipOpts: {content: "%s: $%y.2<br>Verificado el %x"},
                    legend: {
                        show: true,
                        backgroundColor: 'white',
                        noColumns: 7,
                        backgroundOpacity: 0.6
                    }
                });
            }
            else {
                $titulo.html("No se encontró el producto");
            }
        });
    }

    return {
        init: init
    };
})();
$(document).ready(function () {
    prc.ui.producto.init();
});

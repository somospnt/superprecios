prc.service.origen = (function() {
    var origenes = [
        {constante: "CUIDADOS", variable: "precioCuidados", nombre: "Precios Cuidados"},
        {constante: "COTO", variable: "precioCoto", nombre: "Coto"},
        {constante: "DISCO", variable: "precioDisco", nombre: "Disco"},
        {constante: "JUMBO", variable: "precioJumbo", nombre: "Jumbo"},
        {constante: "WALMART", variable: "precioWalmart", nombre: "Walmart"}
    ];

    return {
        origenes: origenes
    };
})();
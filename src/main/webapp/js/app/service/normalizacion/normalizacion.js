prc.service.normalizacion = (function() {

    function guardar(normalizacion) {
        var url = prc.service.url() + "normalizacion/";
        return prc.service.post(url, normalizacion);
    }

    return {
        guardar: guardar
    };
})();
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<header class="ct-pageHeader ct-pageHeader--main sp-pageHeader sp-pageHeader-small ct-u-paddingBottom30">
    <div class="ct-pageHeader-leftSide">
        <img src="${root}/img/icono-con-sombra-rosa.png" alt="Super Precios">
        <h5>Busca un producto y selecciona las descripciones<br>que pertenecen al mismo producto</h5>
    </div>
    <div class="ct-pageHeader-rightSide">
        <div class="sp-pageHeader-buscador">
            <div class="sp-pageHeader-fondo">
                <form class="form-inline" action="${root}/normalizacion.html" method="GET">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" autofocus="autofocus" required="required" name="keywords" size="45" placeholder="�Qu� producto de supermercado busc�s?" value="${keywords}">
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary">
                        <span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Buscar precios
                    </button>
                </form>
            </div>
        </div>
    </div>
</header>


<div class="container-fluid">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <table class="table table-hover sp-lista-productos sp-lista-productos-alt">
                <thead>
                    <tr>
                        <th>Producto seleccionado</th>
                        <c:forEach items="${origenes}" var="origen">
                            <th>${origen.nombre}</th>
                        </c:forEach>
                    </tr>
                </thead>
                <tbody id="lista-productos-iguales">
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-1 col-md-10 text-right">
            <button id="prc-normalizar" type="button" class="btn btn-success">
                <span class="glyphicon glyphicon-check"></span> Son el mismo producto
            </button>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h2>Resultados de la b�squeda</h2>
            <table class="table table-hover sp-lista-productos sp-lista-productos-alt">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <c:forEach items="${origenes}" var="origen">
                            <th>${origen.nombre}</th>
                        </c:forEach>
                    </tr>
                </thead>
                <tbody id="lista-productos-busqueda">
                    <c:forEach items="${productos}" var="producto">
                        <tr class="sp-js-agregar-producto">
                            <td><h5 class="ct-productBox-title text-uppercase sp-js-descripcion">${producto.descripcion}</h5></td>
                            <c:forEach items="${origenes}" var="origen">
                                <td class="sp-lista-productos-precio sp-js-precio-${origen}"><fmt:formatNumber value="${producto.getPrecio(origen)}" type="currency" /></td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>



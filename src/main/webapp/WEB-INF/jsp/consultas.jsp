<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<header class="ct-pageHeader ct-pageHeader--main sp-pageHeader sp-pageHeader-small ct-u-paddingBottom30">
    <div class="ct-pageHeader-leftSide">
        <img src="${root}/img/icono-con-sombra-rosa.png" alt="Super Precios">
    </div>
    <div class="ct-pageHeader-rightSide">
        <div class="sp-pageHeader-buscador">
            <div class="sp-pageHeader-fondo">
                <form class="form-inline" action="${root}/consultas.html" method="GET">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" autofocus="autofocus" required="required" name="keywords" size="45" placeholder="�Qu� producto de supermercado busc�s?" value="${keywords}">
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary">
                        <span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Buscar precios
                    </button>
                </form>
            </div>
        </div>
    </div>
</header>

<section class="ct-u-paddingBottom40">
    <div class="container">
        <div class="text-center ct-u-paddingBottom10">
            <c:if test="${empty productos}">
                <h2>
                    <span class="text-uppercase ct-fw-300">No encontramos productos para tu b�squeda</span><br>
                    <small class="ct-u-colorPrimary">�Quiz�s probando con otras palabras?</small>
                </h2>
            </c:if>
            <c:if test="${not empty productos}">
                <h2>
                    <span class="text-uppercase ct-fw-300">Resultados para "<c:out value="${keywords}"/>"</span><br>
                    <small class="ct-u-colorPrimary">encontramos ${productos.size()} producto${productos.size() > 1 ? "s" : ""}</small>
                </h2>
            </c:if>
        </div>
    </div>
</section>


<%--
<section class="ct-u-paddingBottom50">
    <div class="container">
        <div class="ct-gallery ct-gallery--withSpacing ct-gallery--col4 is-loaded" style="position: relative;">

            <c:forEach items="${productos}" var="producto">
                <div class="ct-gallery-item ct-gallery-item--masonry ct-gallery-item--default ct-gallery-item--normal minty">
                    <div class="ct-gallery-itemInner">
                        <div class="ct-productBox ct-productBox--type1 ct-productBox--green sp-productBox-simple">
                            <div class="ct-productBox-image">
                            </div>
                            <div class="ct-productBox-overlay">
                                <a href="${root}/producto.html?id=${producto.id}">
                                    <div class="ct-productBox-overlayIcon">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                </a>
                            </div>
                            <a href="${root}/producto.html?id=${producto.id}" class="ct-productBox-content">
                                <h4 class="ct-productBox-title text-uppercase"><c:out value="${producto.descripcion}"/></h4>
                                <span class="ct-productBox-description">
                                    <ul class="list-unstyled ct-list-arrowPurple ct-fw-400 text-uppercase sp-js-producto-precios">
                                        <c:forEach items="${origenes}" var="origen">
                                            <c:if test="${not empty producto.getPrecio(origen) }">
                                                <li>${origen.nombre} <span class="sp-productBox-precio sp-js-precio"><fmt:formatNumber value="${producto.getPrecio(origen)}" type="currency" /></span></li>
                                                </c:if>
                                            </c:forEach>
                                    </ul>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </c:forEach>

        </div>
    </div>
</section>
--%>

<section class="ct-u-paddingBottom50">
    <div class="container">
        <table class="table table-hover sp-lista-productos">
            <thead>
                <tr>
                    <th>Producto</th>
                    <c:forEach items="${origenes}" var="origen">
                        <th>${origen.nombre}</th>
                    </c:forEach>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${productos}" var="producto">
                    <tr class="sp-js-producto-precios">
                        <td>
                            <a href="${root}/producto.html?id=${producto.id}" class="ct-productBox-content">
                                <h5 class="ct-productBox-title text-uppercase"><c:out value="${producto.descripcion}"/></h5>
                            </a>
                        </td>
                        <c:forEach items="${origenes}" var="origen">
                            <td class="sp-lista-productos-precio">
                                <c:if test="${not empty producto.getPrecio(origen) }">
                                    <span style="float:none" class="sp-productBox-precio sp-js-precio"><fmt:formatNumber value="${producto.getPrecio(origen)}" type="currency" /></span>
                                </c:if>
                            </td>
                        </c:forEach>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </div>
</section>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 prc-hero prc-hero-alt">
            <h1>Ayudamos a mejorar la informaci�n</h1>
            <form class="form-inline" action="<c:url value="/normalizacion.html"/>" method="GET">
                <div class="form-group">
                    <input type="text" class="form-control" autofocus="autofocus" name="keywords" size="60" placeholder="�Qu� producto busc�s?" value="${keywords}">
                </div>
                <button type="submit" class="btn btn-default">
                    <span class="glyphicon glyphicon-search"></span> Buscar precios
                </button>
            </form>
            <small>* Busca un producto y selecciona las descripciones que son del mismo producto.</small>
        </div>
    </div>
</div>

<div class="container-fluid prc-detalle-normalizacion">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <table class="table table-bordered table-striped prc-lista-precios prc-lista-detalle-normalizacion">
                <thead>
                    <tr>
                        <th><h4>Producto</h4></th>
                        <th><h4>Tambi�n conocido como...</h4></th>
                    </tr>
                </thead>
                <tbody id="lista-productos-iguales">
                    <c:forEach items="${normalizaciones}" var="normalizacion">
                        <tr class="prc-js-normalizacion">
                            <td>
                                <span class="sp-js-descripcion-nueva" data-descripcion="<c:out value="${normalizacion.key}"/>">${normalizacion.key}</span>
                            </td>
                            <td>
                                <c:forEach items="${normalizacion.value}" var="descripcion">
                                    <p><span class="prc-lista-detalle-normalizacion-origen sp-js-descripcion-origen" data-descripcion="<c:out value="${descripcion.descripcionOrigen}"/>">${descripcion.descripcionOrigen}</span></p>
                                </c:forEach>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

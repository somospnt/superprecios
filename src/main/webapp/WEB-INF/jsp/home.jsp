<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<header class="ct-pageHeader ct-pageHeader--main sp-pageHeader ct-u-paddingBottom30">
    <div class="ct-pageHeader-leftSide">
        <img src="${root}/img/icono-con-sombra-rosa.png" alt="Super Precios">
    </div>
    <div class="ct-pageHeader-rightSide">
        <div class="sp-pageHeader-buscador">
            <div class="sp-pageHeader-fondo">
                <form class="form-inline" action="${root}/consultas.html" method="GET">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" autofocus="autofocus" required="required" name="keywords" size="45" placeholder="�Qu� producto de supermercado busc�s?" value="${keywords}">
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary">
                        <span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Buscar precios
                    </button>
                </form>
            </div>
        </div>
    </div>
</header>


<section class="ct-u-paddingBottom20">
    <div class="container">
        <div class="text-center ct-u-paddingBottom10">
            <h2>
                <span class="text-uppercase ct-fw-300">Recorremos los supermercados por vos</span><br>
                <small class="ct-u-colorPrimary">para que encuentres siempre el mejor precio</small>
            </h2>
        </div>
    </div>
</section>

<section class="ct-u-backgroundGray ct-u-paddingBottom30">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--type1 ct-iconBox--primary text-center">
                    <div class="ct-iconBox-icon">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="ct-iconBox-content">
                        <h4 class="ct-iconBox-title text-uppercase">El mejor precio</h4>
                        <p class="ct-iconBox-description">
                            Ahorr� en tus compras cotidianas encontrando el supermercado que te ofrece el mejor precio para los art�culos que compr�s.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--type1 ct-iconBox--green text-center">
                    <div class="ct-iconBox-icon">
                        <i class="fa fa-line-chart"></i>
                    </div>
                    <div class="ct-iconBox-content">
                        <h4 class="ct-iconBox-title text-uppercase">Evoluci�n de precios</h4>
                        <p class="ct-iconBox-description">
                            Guardamos todos los precios para que puedas comparar la evoluci�n de los precios de cada producto en cada supermercado.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--type1 ct-iconBox--purple text-center">
                    <div class="ct-iconBox-icon">
                        <i class="fa fa-gavel"></i>
                    </div>
                    <div class="ct-iconBox-content">
                        <h4 class="ct-iconBox-title text-uppercase">Imparciales</h4>
                        <p class="ct-iconBox-description">
                            Utilizamos una tecnolog�a autom�tica e imparcial para que cuentes con la informaci�n objetiva de los precios.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--type1 ct-iconBox--blue text-center">
                    <div class="ct-iconBox-icon">
                        <i class="fa fa-refresh"></i>
                    </div>
                    <div class="ct-iconBox-content">
                        <h4 class="ct-iconBox-title text-uppercase">Siempre al d�a</h4>
                        <p class="ct-iconBox-description">
                            Relevamos a diario los precios de todos los supermercados para que cuentes con informaci�n precisa y actualizada.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
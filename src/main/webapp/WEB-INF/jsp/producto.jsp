<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<section class="ct-u-paddingBottom20">
    <div class="container">
        <div class="text-center ct-u-paddingBottom10">
            <h2>
                <a class="sp-titulo-volver" href="javascript:history.back()">
                    <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-angle-left fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <span id="prc-js-titulo-producto" class="text-uppercase ct-fw-300">Cargando producto...</span><br>
                <small class="ct-u-colorPrimary">Evolución de precios</small>
            </h2>
        </div>
    </div>
</section>

<div class="container ct-u-paddingBottom50">
    <div class="row">
        <div class="col-md-12">
            <div id="prc-chart" style="width: 100%; height: 400px"></div>
        </div>
    </div>
</div>

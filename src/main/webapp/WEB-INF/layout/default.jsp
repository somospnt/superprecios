<%@page pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c"     uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn"    uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"   uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="es_AR" scope="session"/>

<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="es"> <!--<![endif]-->
    <head lang="es">
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

        <meta charset="UTF-8">
        <meta name="description" content="Super Precios - El buscador de precios de supermercados para Argentina.">
        <meta name="author" content="Somos PNT">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="${root}/favicon.ico" rel="shortcut icon">

        <title>Super Precios - El buscador de precios de supermercados para Argentina</title>

        <link rel="stylesheet" type="text/css" href="${root}/assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="${root}/assets/css/style.css">

        <link rel="stylesheet" type="text/css" href="${root}/css/main.css">

        <!--[if lt IE 9]>
        <script src="${root}/assets/bootstrap/js/html5shiv.min.js"></script>
        <script src="${root}/assets/bootstrap/js/respond.min.js"></script>
        <![endif]-->

        <script src="${root}/assets/js/modernizr.custom.js"></script>
    </head>
    <body class="ct--navbarFixed ct--TopBarFixed ct-js-makeNavbarSmaller ct-js-hideTopBar sp-TopBarHidden">

        <div id="ct-js-wrapper" class="ct-pageWrapper">

            <nav class="navbar navbar-default ct-navbar--logoright sp-navbar" role="navigation">
                <div class="container">
                    <ul class="nav navbar-nav yamm">
                        <li><a class="sp-navbar-brand" href="${root}"><img src="${root}/img/logo-plano-rosa.png" alt="Super Precios"/></a></li>
                    </ul>
                    <div class="pull-right">
                        <ul class="ct-socials list-inline list-unstyled">
                            <li>
                                <a href="https://www.facebook.com/proyectonuevastecnologias" data-toggle="tooltip" data-placement="left" title="" data-original-title="Facebook"><i class="fa fa-fw fa-facebook-square"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/somospnt" data-toggle="tooltip" data-placement="left" title="" data-original-title="Twitter"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>


            <tiles:insertAttribute name="body"/>


            <footer class="ct-u-paddingTop30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h4 class="text-uppercase">Queremos conocerte</h4>
                            <div class="ct-u-paddingTop10">
                                <ul class="list-unstyled ct-contactList">
                                    <li>
                                        <i class="fa fa-fw fa-map-marker"></i>
                                        <span>
                                            Bulnes 2756 <br>
                                            Buenos Aires, Argentina
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-fw fa-phone"></i>
                                        <span>
                                            (+5411) 5556-5100
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-fw fa-send"></i>
                                        <span>
                                            <a href="mailto:contacto@somospnt.com">contacto@somospnt.com</a>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h4 class="text-uppercase">Supermercados Relevados</h4>
                            <div class="ct-u-paddingTop10">
                                <ul class="list-unstyled ct-list-arrowWhite">
                                    <li>Coto</li>
                                    <li>Disco</li>
                                    <li>Jumbo</li>
                                    <li>Walmart</li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix visible-sm"></div>
                        <div class="col-sm-6 col-md-3">
                            <h4 class="text-uppercase">Actualización</h4>
                            <div class="ct-u-paddingTop10">
                                <ul class="list-unstyled ct-list-arrowWhite">
                                    <li>Precios al <fmt:formatDate value="<%=new java.util.Date()%>" pattern="dd 'de' MMMM 'de' yyyy" />.</li>
                                    <li>Más de 35000 productos relevados.</li>
                                    <li>Incluimos Precios Cuidados.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="text-right ct-u-paddingTop60">
                                <img src="${root}/img/icono-con-sombra-violeta.png" alt="Super Precios" width="128">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ct-postFooter ct-u-paddingTop20 ct-u-paddingBottom10 ct-u-marginTop30">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <span class="ct-copyright">Ideado y desarrollado por <a href="http://www.somospnt.com">Somos PNT</a>. El equipo de desarrollo de <a href="http://www.connectis-ict.com.ar">Connectis Argentina</a>.</span>

                                <ul class="list-unstyled list-inline">
                                    <li><a href="mailto:contacto@somospnt.com">Email</a></li>
                                    <li><a href="https://www.facebook.com/proyectonuevastecnologias">Facebook</a></li>
                                    <li><a href="https://twitter.com/somospnt">Twitter</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <a class="ct-scrollUpButton ct-js-btnScrollUp" href="#"><span class="ct-sectionButton-circle"><i class="fa fa-angle-up"></i></span></a>
        </div>


        <script src="${root}/assets/js/jquery.min.js"></script>
        <script src="${root}/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="${root}/assets/js/jquery.placeholder.min.js"></script>
        <script src="${root}/assets/js/jquery.easing.1.3.js"></script>
        <script src="${root}/assets/js/device.min.js"></script>
        <script src="${root}/assets/js/jquery.browser.min.js"></script>
        <script src="${root}/assets/js/snap.min.js"></script>
        <script src="${root}/assets/js/jquery.appear.js"></script>
        <script src="${root}/assets/js/main.js"></script>

        <script src="${root}/js/vendors/jquery-flot/jquery.flot.min.js"></script>
        <script src="${root}/js/vendors/jquery-flot/jquery.flot.time.min.js"></script>
        <script src="${root}/js/vendors/jquery-flot/jquery.flot.tooltip.min.js"></script>
        <script src="${root}/js/vendors/jquery-floatThead/jquery.floatThead.js"></script>

        <%--
        <script src="${root}/assets/js/ct/imageBox.js"></script>

        <script src="${root}/assets/js/flexslider/jquery.flexslider-min.js"></script>
        <script src="${root}/assets/js/flexslider/init.js"></script>

        <script src="${root}/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="${root}/assets/js/magnific-popup/init.js"></script>

        <script src="${root}/assets/js/portfolio/jquery.isotope.min.js"></script>
        <script src="${root}/assets/js/portfolio/imagesloaded.js"></script>
        <script src="${root}/assets/js/portfolio/init.js"></script>
        --%>

        <script src="${root}/js/app/prc.js"></script>
        <script src="${root}/js/app/service/service.js"></script>
        <script src="${root}/js/app/service/normalizacion/normalizacion.js"></script>
        <script src="${root}/js/app/service/origen/origen.js"></script>
        <script src="${root}/js/app/ui/ui.js"></script>
        <tiles:importAttribute name="js" scope="page"/>
        <c:if test="${not empty js}">
            <script src="${root}/${js}"></script>
        </c:if>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-59363316-1', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>

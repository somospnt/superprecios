/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.jumbo;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;
import com.altogamer.wdc.util.HttpUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

public class JumboUrlIterator implements UrlIterator {

    private static final String URL = "https://www.jumboacasa.com.ar/Comprar/HomeService.aspx/ObtenerArticulosPorMenuMarcaFamilia";
    private static final String PAYLOAD = "{code:'ids={id}&cat1=categoria1&cat2=categoria2&lastCat={categoria2}&marca=&producto=&pager=&page=menu&ordenamiento=0'}";
    private static final String[] SELECTORES = new String[] {
        ".categoria-electro",
        ".categoria-textil",
        ".categoria-hogar",
        ".categoria-almacen",
        ".categoria-bebidas",
        ".categoria-frescos",
        ".categoria-congelados",
        ".categoria-elaborados",
        ".categoria-perfumeria",
        ".categoria-limpieza",
        ".categoria-especiales"
    };
    private List<String> ids;
    private int index = 0;
    private int limit = -1;

    @Override
    public void init() {
        index = 0;
        ids = obtenerCategorias();
    }

    @Override
    public synchronized Url next() {
        if ((index >= ids.size() && limit < 0) || (limit >= 0 && index >= limit)) {
            return null;
        }
        String payload = PAYLOAD.replaceAll("\\{id\\}", String.valueOf(ids.get(index)));

        index++;
        return new Url(Url.Method.POST, URL, payload, "application/json");
    }

    @Override
    public boolean allowEmptyResults() {
        return true;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    private List<String> obtenerCategorias() {
        ArrayList<String> idCategorias = new ArrayList<>();
        String html;
        try {
            html = HttpUtils.getBodyFromUrl("https://www.jumboacasa.com.ar/Login/PreHome.aspx");
        } catch (IOException ex) {
            throw new RuntimeException("Error recuperando el menu de Jumbo", ex);
        }
        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(html);
        for (String selector : SELECTORES) {
            doc.$(selector + " ul > li.sub-categorias-contenido ul li a").each(new JerryFunction() {
                @Override
                public boolean onNode(Jerry $this, int index) {
                    String href = $this.attr("href");
                    String id = href.substring(href.indexOf("idlink=") + 7);
                    idCategorias.add(id);
                    return true;
                }
            });
        }
        return idCategorias;
    }


}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.jumbo;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.util.HttpUtils;
import static com.altogamer.wdc.util.HttpUtils.USER_AGENT;
import java.io.IOException;
import jodd.http.HttpBrowser;
import jodd.http.HttpRequest;

public class JumboUrlContentRetriever implements ContentRetriever {

    private HttpBrowser browser;

    @Override
    public void init() {
        browser = new HttpBrowser();
        HttpRequest loginRequest = HttpRequest.get("https://www.jumboacasa.com.ar/Login/PreHome.aspx?ReturnURL=https://www.jumboacasa.com.ar/Comprar/Home.aspx")
                .header("User-Agent", HttpUtils.USER_AGENT);
        browser.sendRequest(loginRequest); //este request setea la cookie de session, necesaria para la consulta posterior
    }

    @Override
    public String getContent(Url url) throws IOException {
        HttpRequest post = HttpRequest.post(url.getUrl())
                .header("User-Agent", USER_AGENT)
                .bodyText(url.getPayload(), url.getContentType());
        return browser.sendRequest(post).bodyText();
    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.jumbo;

import com.altogamer.wdc.Collector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

public class JumboCollector implements Collector {

    private static final int INNER_JSON_START = "{\"accion\":\"busquedaIdMenu\",\"html\":'',\"xml\":'".length();
    private static final int INNER_JSON_END = "'}".length();
    private static final char CHAR_SEPARADOR = 9674;

    @Override
    public List<Producto> collect(String json) throws IOException {
        final List<Producto> results = new ArrayList<>();
        if (json.isEmpty()) {
            return results;
        }

        ObjectMapper mapper = new ObjectMapper();
        String innerJson = mapper.readTree(json).get("d").asText();
        String xml = innerJson.substring(INNER_JSON_START, innerJson.length() - INNER_JSON_END);

        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(xml);
        doc.$("x[for='gondola'] items").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int index) {
                String lineaProductos = $this.html();
                if (lineaProductos.isEmpty()) {
                    //algunas url del servicio devuelven paginas vacias
                    return true;
                }
                String[] productosString = lineaProductos.split("¬");
                for (String productoString : productosString) {
                    String[] datos = productoString.split("" + CHAR_SEPARADOR);
                    Producto producto = new Producto();
                    producto.setPrecio(new BigDecimal(datos[3]), Origen.JUMBO);
                    producto.setDescripcion(datos[2]);
                    producto.setVigente(true);
                    results.add(producto);
                }
                return true;
            }
        });

        return results;
    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.cuidados;

import com.altogamer.wdc.Collector;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CuidadosCollector implements Collector {

    private static final Logger logger = LoggerFactory.getLogger(CuidadosCollector.class);

    @Override
    public List<Producto> collect(String html) throws IOException {
        final List<Producto> results = new ArrayList<>();
        if (html.isEmpty()) {
            return results;
        }

        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(html);
        doc.$(".productoIndividual").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int index) {
                Producto producto = new Producto();
                StringBuilder descripcion = new StringBuilder($this.find("div.detalle > table > tbody > tr:nth-child(1) > td").text().trim());
                descripcion.append(" ");
                descripcion.append($this.find("div.detalle > table > tbody > tr:nth-child(2) > td").text().trim());

                String marca = $this.find("div.detalle > table > tbody > tr:nth-child(4) > td").text().trim();
                if (!marca.equals("SIN MARCA (SUPERMERCADO)")) {
                    descripcion.append(" ");
                    descripcion.append($this.find("div.detalle > table > tbody > tr:nth-child(4) > td").text().trim());
                }

                producto.setDescripcion(descripcion.toString());
                producto.setPrecio(getPrecio($this.find("table.precio > tbody > tr > td.text-right > strong").text()), Origen.CUIDADOS);
                producto.setVigente(true);
                results.add(producto);
                return true;
            }

        });

        return results;
    }

    private BigDecimal getPrecio(String precio) {
        try {
            precio = precio.trim();
            precio = precio.replaceAll("\\$", ""); //quitar el simoblo $ del principio
            precio = precio.replaceAll(",", "."); //reemplaza coma por punto
            return new BigDecimal(precio);
        } catch (NumberFormatException ex) {
            logger.error("No se puede parsear el precio: " + precio, ex);
            return null;
        }
    }

}

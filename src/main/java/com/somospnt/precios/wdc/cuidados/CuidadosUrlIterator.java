/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.cuidados;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;

public class CuidadosUrlIterator implements UrlIterator {

    private static final String URL = "http://precioscuidados.gob.ar/productos-supermercados";
    private static final String PAYLOAD = "form[province]=2&form[district]=5";

    private int index = 0;

    @Override
    public void init() {
        index = 0;
    }

    @Override
    public synchronized Url next() {
        if (index > 0 ) {
            return null;
        }
        index++;
        return new Url(Url.Method.POST, URL, PAYLOAD, "application/x-www-form-urlencoded");
    }

    @Override
    public boolean allowEmptyResults() {
        return true;
    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.disco;

import com.altogamer.wdc.Collector;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiscoCollector implements Collector {

    private static final Logger logger = LoggerFactory.getLogger(DiscoCollector.class);

    @Override
    public List<Producto> collect(String html) throws IOException {
        final List<Producto> results = new ArrayList<>();
        if (html.isEmpty()) {
            return results;
        }
        html = html.replaceAll("\\\\\"", "\"");

        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(html);
        doc.$("table tr.filaListaDetalle").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int index) {
                Producto producto = new Producto();
                producto.setDescripcion($this.find("td a").text().trim());
                producto.setPrecio(getPrecio($this.find("td:nth-child(7)").text()), Origen.DISCO);
                producto.setVigente(true);
                results.add(producto);
                return true;
            }

        });

        return results;
    }

    private BigDecimal getPrecio(String precio) {
        try {
            precio = precio.trim();
            precio = precio.replaceAll("\\$", ""); //quitar el simoblo $ del principio
            return new BigDecimal(precio);
        } catch (NumberFormatException ex) {
            logger.error("No se puede parsear el precio: " + precio, ex);
            return null;
        }
    }

}

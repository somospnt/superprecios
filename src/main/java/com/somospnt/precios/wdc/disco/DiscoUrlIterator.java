/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.disco;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;
import static com.altogamer.wdc.util.HttpUtils.USER_AGENT;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jodd.http.HttpBrowser;
import jodd.http.HttpRequest;

public class DiscoUrlIterator implements UrlIterator {

    private static final String URL = "https://www3.discovirtual.com.ar/ajaxpro/_MasterPages_Home,DiviComprasWeb.ashx?method=MostrarGondola";
    private static final String PAYLOAD = "{\"idMenu\":{id}}";
    private List<String> ids;
    private int index = 0;
    private int limit = -1;

    @Override
    public void init() {
        index = 0;
        ids = new ArrayList<>();
        String javascript;
        HttpBrowser browser = new HttpBrowser();

        HttpRequest loginRequest = HttpRequest.get("https://www3.discovirtual.com.ar/Login/Invitado.aspx");
        browser.sendRequest(loginRequest); //este request setea la cookie de session, necesaria para la consulta posterior

        HttpRequest postRequest = HttpRequest.post("https://www3.discovirtual.com.ar/Comprar/Menu.aspx?IdLocal=9235&IdTipoCompra=4")
                .header("User-Agent", USER_AGENT)
                .bodyText(PAYLOAD, "text/plain; charset=UTF-8");

        javascript = browser.sendRequest(postRequest).bodyText();

        //parsea lineas del estilo "g[0][2][8][2][2][2][2][2][3] = new Array(2758,'Ceras',null);"
        //las que terminan con el texto "null);" son las hojas del menu, las que sirven para procesar
        Pattern pattern = Pattern.compile("(\\d{4,9}).*null\\);");
        Matcher matcher = pattern.matcher(javascript);
        while (matcher.find()) {
            String idMenu = matcher.group(1);
            ids.add(idMenu);
        }

    }

    @Override
    public synchronized Url next() {
        if ((index >= ids.size() && limit < 0) || (limit >= 0 && index >= limit)) {
            return null;
        }
        String payload = PAYLOAD.replaceAll("\\{id\\}", String.valueOf(ids.get(index)));
        index++;
        return new Url(Url.Method.POST, URL, payload, "application/json");
    }

    @Override
    public boolean allowEmptyResults() {
        return true;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

}

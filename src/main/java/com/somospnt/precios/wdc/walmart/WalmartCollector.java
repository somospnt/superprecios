/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.walmart;

import com.altogamer.wdc.Collector;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WalmartCollector implements Collector {

    private static final Logger logger = LoggerFactory.getLogger(WalmartCollector.class);

    @Override
    public List<Producto> collect(String json) throws IOException {
        final List<Producto> results = new ArrayList<>();
        if (json.isEmpty()) {
            return results;
        }

        ObjectMapper mapper = new ObjectMapper();
        Iterator<JsonNode> elements = mapper.readTree(json).elements();

        while (elements.hasNext()) {
            JsonNode element = elements.next();
            Producto producto = new Producto();

            producto.setDescripcion(element.get("Description").asText().trim());
            producto.setPrecio(getPrecio(element.get("Precio").asText()), Origen.WALMART);
            producto.setVigente(true);
            if (producto.getPrecioWalmart().doubleValue() > 0) {
                results.add(producto);
            }
            else {
                logger.warn("Se encontro un precio en cero, ignorarando producto: " + producto);
            }
        }

        return results;
    }

    private BigDecimal getPrecio(String precio) {
        try {
            precio = precio.trim();
            precio = precio.replaceAll("\\$", ""); //quitar el simoblo $ del principio
            precio = precio.replaceAll(",", "");   //quitar los separadores de miles
            return new BigDecimal(precio);
        }
        catch (NumberFormatException ex) {
            logger.error("No se puede parsear el precio: " + precio, ex);
            return null;
        }
    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.walmart;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;
import com.altogamer.wdc.util.HttpUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import jodd.json.JsonParser;

public class WalmartUrlIterator implements UrlIterator {

    private static final String BASE_URL = "https://walmartonline.com.ar/WebControls/hlSearchResults.ashx?busqueda=undefined&departamento={departamento}&familia=undefined&linea=undefined&orderby=undefined&orderbyid=undefined&range=undefined&sid=2.6403622487023384";
    private List<String> ids;

    private int index = 0;
    private int limit = -1;

    @Override
    public void init() {
        index = 0;
        ids = new ArrayList<>();
        String json;

        try {
            json = HttpUtils.getBodyFromUrl("https://walmartonline.com.ar/WebControls/hlMenuLeft.ashx");
            Map<String,Object> menu = new JsonParser().parse(json);
            List<Map> elementos = (List<Map>)((List<Map>) menu.get("MenuPrincipal")).get(0).get("Elements");
            elementos.stream().forEach((elemento) -> {
                String departamento = (String) elemento.get("departmentName");
                departamento = departamento.replaceAll(" ", "%20");
                ids.add(departamento);
            });

        } catch (IOException ex) {
            throw new RuntimeException("Error recuperando el menu de Walmart", ex);
        }
    }

    @Override
    public synchronized Url next() {
        if ((index >= ids.size() && limit < 0) || (limit >= 0 && index >= limit)) {
            return null;
        }
        return new Url(Url.Method.GET, BASE_URL.replaceAll("\\{departamento\\}", ids.get(index++)));
    }

    @Override
    public boolean allowEmptyResults() {
        return true;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

}

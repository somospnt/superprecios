/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc;

import com.altogamer.wdc.Processor;
import com.altogamer.wdc.Url;
import com.somospnt.precios.domain.Normalizacion;
import com.somospnt.precios.domain.Producto;
import com.somospnt.precios.repository.NormalizacionRepository;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Guarda en memoria los productos, asegurandose que sean unicos. Si se reciben
 * productos duplicados, se queda con el ultimo recibido (y se pierde el
 * anterior). Esto es util porque algunos supermercados tienen el mismo producto
 * en varias categorias.
 */
@Service
public class ProductosProcessor implements Processor {

    private final Map<String, Producto> productos = new ConcurrentHashMap<>();
    private Map<String, Normalizacion> normalizaciones = new ConcurrentHashMap<>();

    @Autowired
    private NormalizacionRepository normalizacionRepository;

    @Override
    public void init() {
        normalizaciones = normalizacionRepository.findAll(new Sort(Sort.Direction.ASC, "descripcionNueva")).stream().collect(Collectors.toConcurrentMap(Normalizacion::getDescripcionOrigen, Function.identity()));
    }

    @Override
    public void process(List items, Url url) {
        List<Producto> productosAProcesar = items;
        productosAProcesar.stream().forEach(producto -> {
            normalizarDescripciones(producto);
            productos.put(producto.getDescripcion(), producto);
        });
    }

    /**
     * Unifica las descripciones del producto, buscando si tiene una descripción
     * alternativa unificada.
     */
    private void normalizarDescripciones(Producto producto) {
        Normalizacion normalizacion = normalizaciones.get(producto.getDescripcion());
        if (normalizacion != null) {
            producto.setDescripcion(normalizacion.getDescripcionNueva());
        }
    }

    public void clear() {
        productos.clear();
    }

    public Collection<Producto> getProductos() {
        return productos.values();
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.coto;

import com.altogamer.wdc.Collector;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

public class CotoCollector implements Collector {

    @Override
    public List<Producto> collect(String html) throws IOException {
        final List<Producto> results = new ArrayList<>();
        if (html.isEmpty()) {
            return results;
        }

        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(html);
        doc.$("script").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int index) {
                if (index != 13) {
                    return true;
                }
                String js = $this.html().trim();
                String[] lineas = js.split("\r");
                String[] precios = parsePrecios(lineas[1]);
                String[] descripciones = parseDescripciones(lineas[3]);

                for (int i = 0; i < descripciones.length - 1; i++) {
                    Producto producto = new Producto();
                    producto.setDescripcion(descripciones[i]);
                    producto.setPrecio(new BigDecimal(precios[i]), Origen.COTO);
                    producto.setVigente(true);
                    results.add(producto);
                }

                return true;
            }
        });

        return results;
    }

    /**
     * Obtiene los precios. Parsea un texto del estilo:
     *     var cart = new Array(8.25,8.57,8.57,8.57,10.59,10.59,6.15,10.59,6.15,6.15,13.5,9.2,13.5,9.2,17.77,17.77,13.5,17.77,9.2,13.5,17.77,9.2,13.5,9.2,13.93,17.33,15.19,16.8,16.8,15.19,0);
     *
     */
    private static String[] parsePrecios(String linea) {
        String inicio = "var cart = new Array(";
        String fin = ");";
        String lineaPrecios = linea.substring(inicio.length(), linea.lastIndexOf(fin));
        String[] precios = lineaPrecios.split(",");
        return precios;
    }

    /**
     * Obtiene las descripciones. Parsea un texto del estilo:
     *     var desc = new Array('Agua con gas &lt;B&gt;CELLIER&lt;/B&gt; naranja durazno bot 1.5 ltr','Agua con gas &lt;B&gt;COTO&lt;/B&gt; citrus bot 1,5 ltr','Agua con gas &lt;B&gt;COTO&lt;/B&gt; lima limon bot 1,5 ltr','Agua con gas &lt;B&gt;COTO&lt;/B&gt; naranja durazno bot 1,5 ltr','Agua con gas cunington style pomelo bot 2 lt','Agua con gas &lt;B&gt;CUNNINGTON&lt;/B&gt; citrus bot 2 ltr','Agua con gas &lt;B&gt;CUNNINGTON&lt;/B&gt; citrus pet 600 cc','Agua con gas &lt;B&gt;CUNNINGTON&lt;/B&gt; naranja durazno bot 2  ltr','Agua con gas &lt;B&gt;CUNNINGTON&lt;/B&gt; style durazno-naranja pet 600 cc','Agua con gas &lt;B&gt;CUNNINGTON&lt;/B&gt; style pomelo pet 600 cc','Agua con gas &lt;B&gt;H2OH&lt;/B&gt; citrus bot 1,5 ltr','Agua con gas &lt;B&gt;H2OH&lt;/B&gt; citrus bot 500 cc','Agua con gas &lt;B&gt;H2OH&lt;/B&gt; lima limon bot 1,5 ltr','Agua con gas &lt;B&gt;H2OH&lt;/B&gt; lima limon bot 500 cc','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; citrus bot 2.25 ltr','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; lima limon bot 2.25 ltr','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; limoneto bot 1.5 ltr','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; limoneto bot 2.25 ltr','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; limoneto bot 500 cc','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; naranchelo bot 1.5 ltr','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; naranchelo bot 2.25 lt','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; naranchelo bot 500 cc','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; pomelo pink bot 1.5 ltr','Agua con gas &lt;B&gt;H2OH!!&lt;/B&gt; pomelo pink bot 500 cc','Agua con gas &lt;B&gt;SER&lt;/B&gt; citrus bot 1,5 ltr','Agua con gas &lt;B&gt;SER&lt;/B&gt; citrus bot 2.25 ltr','Agua con gas ser citrus &lt;B&gt;WE&lt;/B&gt; bot 1.5 ltr','Agua con gas ser citrus &lt;B&gt;WE&lt;/B&gt; bot 2.25 ltr','Agua con gas ser lemon &lt;B&gt;WE&lt;/B&gt; bot 2.25 ltr','Agua con gas ser limon &lt;B&gt;WE&lt;/B&gt; bot 1.5 ltr','');
     */
    private static String[] parseDescripciones(String linea) {
        String inicio = "var desc = new Array(";
        String fin = ");";

        linea = linea.substring(inicio.length(), linea.lastIndexOf(fin));
        linea = linea.replaceAll("&lt;B&gt;", "").replaceAll("&lt;/B&gt;", "");  //quitar los tags de negrita

        String[] descripciones = linea.split("','"); //evitar split solo por "'" por si aparece en una descripcion
        if (descripciones.length > 0) {
            //quitar el primer apostrofe del string, porque el split no lo toma
            descripciones[0] = descripciones[0].substring(1);
        }
        return descripciones;
    }

}

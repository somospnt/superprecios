/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.coto;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;
import com.altogamer.wdc.util.HttpUtils;
import static com.altogamer.wdc.util.HttpUtils.USER_AGENT;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jodd.http.HttpBrowser;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

public class CotoUrlIterator implements UrlIterator {

    private static final String URL = "http://www.cotodigital.com.ar/l.asp?cat={id}&id={id}&start={desde}";
    private static final String[] desde = new String[] {"1", "31", "61", "91"}; //suponer 4 paginas
    private List<String> ids;
    private int index = 0;
    private int indexDesde = 0;
    private int limit = -1;

    @Override
    public void init() {
        index = 0;
        indexDesde = 0;
        ids = buscarIdCategorias();
    }

    @Override
    public synchronized Url next() {
        if ((index >= ids.size() && limit < 0) || (limit >= 0 && index >= limit)) {
            return null;
        }
        String url = URL.replaceAll("\\{id\\}", String.valueOf(ids.get(index)));
        url = url.replaceAll("\\{desde\\}", desde[indexDesde]);
        indexDesde++;
        if (indexDesde >= desde.length) {
            indexDesde = 0;
            index++;
        }
        return new Url(Url.Method.GET, url);
    }

    @Override
    public boolean allowEmptyResults() {
        return true;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }


    /**
     * Busca las categorias, parseando el menu del sitio.
     * Parsea lineas del estilo:
     *     g[1] = new Array(1378,'Aire Libre', 1377, '');g[2] = new Array(1638,'Camping', 1377, '');g[3] = new Array(1379,'Deportes', 1377, '');
     *
     * En el ejemplo:
     *     1378 es el codigo de la categoria.
     *     Aire Libre es la descripcion.
     *     1377 es el codigo de la categoria padre.
     *
     * Solo sirven las categorias "hoja" (que no tienen hijos). Este metodo se
     * encarga de buscar categorias sin hijos y devolverlas.
     *
     */
    private List<String> buscarIdCategorias() {
        HttpBrowser browser = new HttpBrowser();
        HttpResponse loginResponse = HttpRequest.get("http://www.cotodigital.com.ar/entrada_guardarconfig.asp?scw=1600&sch=900&nav=NS6&an=Netscape&av=5.0%20%28Windows%29&srb=24&ac=&flash=1&referer=http://www.cotodigital.com.ar/default.asp&azar=172432357")
                .header("User-Agent", HttpUtils.USER_AGENT)
                .send();
        String cookie = loginResponse.header("Set-Cookie");
        HttpRequest post = HttpRequest.get("http://www.cotodigital.com.ar/novedades.asp")
                .header("User-Agent", USER_AGENT)
                .header("Cookie", cookie);
        String html = browser.sendRequest(post).body();

        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(html);
        Set<String> idCategorias = new HashSet<>();
        doc.$("script").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int index) {
                if (index != 8) {
                    return true;
                }
                String js = $this.html().trim();
                String[] lineas = js.split("\r\n");
                for (String linea : lineas) {
                    if (linea.startsWith("g[")) {
                        String[] categoriasJs = linea.split(";");
                        for (String lineaCategoria : categoriasJs) {
                            String inicio = "new Array(";
                            lineaCategoria = lineaCategoria.substring(lineaCategoria.indexOf(inicio) + inicio.length());
                            String[] datosCategoria = lineaCategoria.split(",");
                            String idCategoria = datosCategoria[0].trim();
                            String idCategoriaPadre = datosCategoria[2].trim();
                            idCategorias.remove(idCategoriaPadre); //eliminar la categoria que es padre, solo queremos las hojas
                            idCategorias.add(idCategoria);
                        }
                    }
                }
                return true;
            }
        });

        return new ArrayList<>(idCategorias);
    }

}

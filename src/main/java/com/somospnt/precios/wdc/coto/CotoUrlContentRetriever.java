/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.wdc.coto;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.ContentRetriever;
import com.altogamer.wdc.util.HttpUtils;
import static com.altogamer.wdc.util.HttpUtils.USER_AGENT;
import java.io.IOException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

public class CotoUrlContentRetriever implements ContentRetriever {

    private String cookie;

    @Override
    public void init() {
        HttpResponse loginResponse = HttpRequest.get("http://www.cotodigital.com.ar/entrada_guardarconfig.asp?scw=1600&sch=900&nav=NS6&an=Netscape&av=5.0%20%28Windows%29&srb=24&ac=&flash=1&referer=http://www.cotodigital.com.ar/default.asp&azar=172432357")
                .header("User-Agent", HttpUtils.USER_AGENT)
                .send();
        cookie = loginResponse.header("Set-Cookie");
    }

    @Override
    public String getContent(Url url) throws IOException {
        return HttpRequest.get(url.getUrl())
                .header("User-Agent", USER_AGENT)
                .header("Cookie", cookie)
                .send().bodyText();
    }

}

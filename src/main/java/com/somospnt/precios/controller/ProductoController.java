/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.controller;

import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import com.somospnt.precios.service.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @RequestMapping("/consultas.html")
    public String consultas(@RequestParam(value = "keywords", required = false) String keywords, Model model) {
        if (keywords != null) {
            List<Producto> productos = productoService.buscarPorKeywords(keywords);
            model.addAttribute("productos", productos);
            model.addAttribute("origenes", Origen.values());
        }
        model.addAttribute("keywords", keywords);
        return "consultas";
    }

    @RequestMapping("/producto.html")
    public String productoDetalle() {
        return "producto";
    }

}

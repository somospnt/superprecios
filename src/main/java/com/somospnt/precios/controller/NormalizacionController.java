/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.controller;

import com.somospnt.precios.domain.Normalizacion;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.service.NormalizacionService;
import com.somospnt.precios.service.ProductoService;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NormalizacionController {

    @Autowired
    private ProductoService productoService;

    @Autowired
    private NormalizacionService normalizacionService;


    @RequestMapping("/normalizacion.html")
    public String consultas(@RequestParam(value = "keywords", required = false) String keywords, Model model) {
        if (keywords != null) {
            model.addAttribute("productos", productoService.buscarPorKeywords(keywords));
        }
        model.addAttribute("origenes", Origen.values());
        model.addAttribute("keywords", keywords);
        model.addAttribute("normalizacion", true);
        return "normalizacion";
    }

    @RequestMapping("normalizacion/detalle.html")
    public String buscarNormalizaciones(Model model) {
        model.addAttribute("normalizaciones", normalizacionService.buscarTodas().stream().collect(Collectors.groupingBy(Normalizacion::getDescripcionNueva,Collectors.toList())));
        return "detalle-normalizacion";
    }


}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.controller;

import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import com.somospnt.precios.service.ProductoSchedulerService;
import com.somospnt.precios.service.ProductoService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductoRestController {

    @Autowired
    private ProductoService productoService;
    @Autowired
    private ProductoSchedulerService productoSchedulerService;

    @RequestMapping("/api/productos/actualizar")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public synchronized void actualizarTodo(HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.write("Actualizando... ");
        response.flushBuffer();
        productoSchedulerService.actualizarTodos();
        writer.write("Finalizado.");
    }

    @RequestMapping("/api/productos/{idProducto}/historicos")
    public List<Producto> buscarHistoricos(@PathVariable long idProducto) {
        return productoService.buscarHistorico(idProducto);
    }

    @RequestMapping("/api/productos")
    public Map<Origen, List<Producto>> buscarPorKeywords(@RequestParam(value = "keywords", required = false) String keywords) {
        List<Producto> productos = productoService.buscarPorKeywords(keywords);
        Map<Origen, List<Producto>> productosPorOrigen = new HashMap<>();
        for (Origen origen : Origen.values()) {
            productosPorOrigen.put(origen, new ArrayList<>());
        }
        productos.stream().forEach(producto -> {
            for (Origen origen : Origen.values()) {
                if (producto.getPrecio(origen) != null) {
                    productosPorOrigen.get(origen).add(producto);
                }
            }
        });
        return productosPorOrigen;
    }
}

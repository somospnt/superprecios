/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.controller;

import com.somospnt.precios.domain.Normalizacion;
import com.somospnt.precios.service.NormalizacionService;
import com.somospnt.precios.vo.NormalizacionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NormalizacionRestController {

    @Autowired
    private NormalizacionService normalizacionService;

    @RequestMapping(value = "/api/normalizacion", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void guardar(@RequestBody NormalizacionVo normalizacionVo) {
        normalizacionService.guardar(normalizacionVo);
    }

}

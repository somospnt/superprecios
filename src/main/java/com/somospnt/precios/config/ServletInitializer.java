package com.somospnt.precios.config;

import com.somospnt.precios.ApplicationConfig;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * Esta es la clase que inicializa la aplicacion web con Spring Boot.
 * Levanta la clase de configuración principal de la aplicación.
 */
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApplicationConfig.class);
    }

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.config;

import com.altogamer.wdc.Processor;
import com.altogamer.wdc.WebCrawler;
import com.altogamer.wdc.impl.SimpleUrlContentRetriever;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.wdc.coto.CotoCollector;
import com.somospnt.precios.wdc.coto.CotoUrlContentRetriever;
import com.somospnt.precios.wdc.coto.CotoUrlIterator;
import com.somospnt.precios.wdc.cuidados.CuidadosCollector;
import com.somospnt.precios.wdc.cuidados.CuidadosUrlIterator;
import com.somospnt.precios.wdc.disco.DiscoCollector;
import com.somospnt.precios.wdc.disco.DiscoUrlContentRetriever;
import com.somospnt.precios.wdc.disco.DiscoUrlIterator;
import com.somospnt.precios.wdc.jumbo.JumboCollector;
import com.somospnt.precios.wdc.jumbo.JumboUrlContentRetriever;
import com.somospnt.precios.wdc.jumbo.JumboUrlIterator;
import com.somospnt.precios.wdc.walmart.WalmartCollector;
import com.somospnt.precios.wdc.walmart.WalmartUrlContentRetriever;
import com.somospnt.precios.wdc.walmart.WalmartUrlIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CrawlerConfig {

    @Autowired
    private Processor processor;
    @Value("${com.somospnt.precios.crawler.poolsize}")
    private int poolSize;

    @Bean
    public WebCrawler coto() {
        WebCrawler crawler = crearCrawler();
        crawler.setCollector(new CotoCollector());
        crawler.setUrlIterator(new CotoUrlIterator());
        crawler.setContentRetriever(new CotoUrlContentRetriever());
        crawler.addCustomProperty("origen", Origen.COTO);
        return crawler;
    }

    @Bean
    public WebCrawler disco() {
        WebCrawler crawler = crearCrawler();
        crawler.setCollector(new DiscoCollector());
        crawler.setUrlIterator(new DiscoUrlIterator());
        crawler.setContentRetriever(new DiscoUrlContentRetriever());
        crawler.addCustomProperty("origen", Origen.DISCO);
        return crawler;
    }

    @Bean
    public WebCrawler jumbo() {
        WebCrawler crawler = crearCrawler();
        crawler.setCollector(new JumboCollector());
        crawler.setUrlIterator(new JumboUrlIterator());
        crawler.setContentRetriever(new JumboUrlContentRetriever());
        crawler.addCustomProperty("origen", Origen.JUMBO);
        return crawler;
    }

    @Bean
    public WebCrawler preciosCuidados() {
        WebCrawler crawler = crearCrawler();
        crawler.setCollector(new CuidadosCollector());
        crawler.setUrlIterator(new CuidadosUrlIterator());
        crawler.setContentRetriever(new SimpleUrlContentRetriever());
        crawler.addCustomProperty("origen", Origen.CUIDADOS);
        return crawler;
    }

    @Bean
    public WebCrawler walmart() {
        WebCrawler crawler = crearCrawler();
        crawler.setCollector(new WalmartCollector());
        crawler.setUrlIterator(new WalmartUrlIterator());
        crawler.setContentRetriever(new WalmartUrlContentRetriever());
        crawler.addCustomProperty("origen", Origen.WALMART);
        return crawler;
    }

    private WebCrawler crearCrawler() {
        WebCrawler crawler = new WebCrawler();
        crawler.setProcessor(processor);
        crawler.setCorePoolSize(poolSize);
        crawler.setMaximumPoolSize(poolSize);
        return crawler;
    }

}

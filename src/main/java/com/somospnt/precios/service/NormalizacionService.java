/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.service;

import com.somospnt.precios.domain.Normalizacion;
import com.somospnt.precios.vo.NormalizacionVo;
import java.util.List;

public interface NormalizacionService {

    void guardar(NormalizacionVo normalizacionVo);
    List<Normalizacion> buscarTodas();

}

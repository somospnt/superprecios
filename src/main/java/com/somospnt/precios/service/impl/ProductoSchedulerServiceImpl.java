/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.service.impl;

import com.altogamer.wdc.WebCrawler;
import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import com.somospnt.precios.repository.ProductoRepository;
import com.somospnt.precios.service.ProductoSchedulerService;
import com.somospnt.precios.wdc.ProductosProcessor;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.Normalizer;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductoSchedulerServiceImpl implements ProductoSchedulerService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ProductoSchedulerService.class);

    @Autowired
    private List<WebCrawler> crawlers;

    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private ProductosProcessor processor;

    @Scheduled(cron = "0 0 2 * * ?")
    @Override
    public void actualizarTodos() {
        long inicio = System.currentTimeMillis();
        logger.info("Comenzando actualizacion general...");
        Map<String, Producto> productosRecolectados = new HashMap<>();
        Date fecha = new Date();
        try {
            for (WebCrawler crawler : crawlers) {
                processor.clear();
                crawler.run();
                recolectarProductos(productosRecolectados, processor.getProductos(), (Origen) crawler.getCustomProperty("origen"), fecha);
            }
            logger.info("Guardando {} productos en la base de datos...", productosRecolectados.size());
            productoRepository.actualizarParaNoVigente();
            productoRepository.save(productosRecolectados.values());
        } catch (IOException | InterruptedException ex) {
            logger.error("Error en actualizacion general", ex);
            throw new RuntimeException(ex);
        } finally {
            processor.clear();
            long total = System.currentTimeMillis() - inicio;
            logger.info("Fin de actualizacion general en {}ms.", total);
        }
    }

    private void recolectarProductos(Map<String, Producto> productosRecolectados, Collection<Producto> productos, Origen origen, Date fecha) {
        productos.stream().forEach(producto -> {
            producto.setFecha(fecha);
            String claveUnica = crearClaveUnicaDeProducto(producto);
            Producto productoExistente = productosRecolectados.get(claveUnica);
            if (productoExistente == null) {
                productosRecolectados.put(claveUnica, producto);
            }
            else {
                BigDecimal precioExistente = productoExistente.getPrecio(origen);
                BigDecimal precioNuevo = producto.getPrecio(origen);
                if (precioExistente == null || precioExistente.compareTo(precioNuevo) < 0) {
                    //Hay productos duplicados en los supermercados, y tienen precio distinto. Nos quedamos con el precio mas alto.
                    productoExistente.setPrecio(precioNuevo, origen);
                }
            }
        });
    }

    private static String crearClaveUnicaDeProducto(Producto producto) {
        return Normalizer.normalize(producto.getDescripcion(), Normalizer.Form.NFD) //quitar acentos
                .trim()                                 //" The Game - Episode II: Get & Tannen! " --> "The Game - Episode II: Get & Tannen!"
                .toLowerCase()                          //"The Game - Episode II: Get & Tannen!"   --> "the game - episode ii: get & tannen!"
                .replaceAll("\\s*[-]+\\s*", "-")        //"the game - episode ii: get & tannen!"   --> "the game-episode ii: get & tannen!"
                .replaceAll("\\s*&\\s*", " and ")       //"the game - episode ii: get & tannen!"   --> "the game-episode ii: get and tannen!"
                .replaceAll("[^\\dA-Za-z -]", "")       //"the game-episode ii: get and tannen!"   --> "the game-episode ii get and tannen"
                .replaceAll("\\s+", "-");               //"the game-episode ii get and tannen"     --> "the-game-episode-ii-get-and-tannen"
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.service.impl;

import com.somospnt.precios.domain.Origen;
import com.somospnt.precios.domain.Producto;
import com.somospnt.precios.repository.ProductoRepository;
import com.somospnt.precios.service.ProductoService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    @Override
    public List<Producto> buscarPorKeywords(String keywords) {
        String[] palabras = keywords.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String palabra : palabras) {
            if (!palabra.isEmpty()) {
                builder.append(" +");
                builder.append(palabra);
            }
        }
        return productoRepository.findByText(builder.toString());
    }

    @Override
    public List<Producto> buscarHistorico(long idProducto) {
        Producto producto = productoRepository.findOne(idProducto);
        if (producto == null) {
            return new ArrayList<>();
        } else {
            return productoRepository.findByDescripcionIgnoreCaseOrderByFechaAsc(producto.getDescripcion());
        }
    }

    @Override
    public void aplicarNormalizacion(String descripcionNueva, List<String> descripcionesOrigen) {

        Optional<Producto> productoDestino = productoRepository.findByDescripcionIgnoreCaseAndVigente(descripcionNueva, true);

        // En el caso que se cree una descripción propia que no exista
        if (!productoDestino.isPresent()) {
            Producto producto = new Producto();
            producto.setDescripcion(descripcionNueva);
            producto.setFecha(new Date());
            producto.setVigente(true);
            productoDestino = Optional.of(producto);
        }

        for (String descripcionOrigen : descripcionesOrigen) {
            productoDestino.ifPresent(pDestino -> {
                if (!descripcionNueva.equals(descripcionOrigen)) {
                    Optional<Producto> productoOrigen = productoRepository.findByDescripcionIgnoreCaseAndVigente(descripcionOrigen, true);
                    productoOrigen.ifPresent(pOrigen -> {
                        for (Origen origen : Origen.values()) {
                            BigDecimal precio = pOrigen.getPrecio(origen);
                            if (precio != null) {
                                pDestino.setPrecio(precio, origen);
                            }
                        }
                        productoRepository.delete(pOrigen);
                    });
                    productoRepository.save(pDestino);
                }
            });
        }
    }
}

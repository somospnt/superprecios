/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.service.impl;

import com.somospnt.precios.domain.Normalizacion;
import com.somospnt.precios.repository.NormalizacionRepository;
import com.somospnt.precios.service.NormalizacionService;
import com.somospnt.precios.service.ProductoService;
import com.somospnt.precios.vo.NormalizacionVo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class NormalizacionServiceImpl implements NormalizacionService {

    @Autowired
    private NormalizacionRepository normalizacionRepository;

    @Autowired
    private ProductoService productoService;

    @Override
    public void guardar(NormalizacionVo normalizacionVo) {

        normalizacionVo.getDescripciones().stream().forEach(descripcion -> {
            // Solo procesamos los registros que difieren de la nueva descripción.
            if (!normalizacionVo.getDescripcion().equals(descripcion)) {
                List<Normalizacion> normalizaciones = normalizacionRepository.findByDescripcionNueva(descripcion);
                if (normalizaciones.isEmpty()) {
                    Normalizacion normalizacion = new Normalizacion();
                    normalizacion.setDescripcionNueva(normalizacionVo.getDescripcion());
                    normalizacion.setDescripcionOrigen(descripcion);
                    productoService.aplicarNormalizacion(normalizacionVo.getDescripcion(), normalizacionVo.getDescripciones());
                    normalizacionRepository.save(normalizacion);
                } else {
                    throw new RuntimeException("Ya existe una normalización con la descripción seleccionada.");
                }

            }

        });

    }

    @Override
    public List<Normalizacion> buscarTodas() {
        return normalizacionRepository.findAll(new Sort(Sort.Direction.ASC, "descripcionNueva"));
    }

}

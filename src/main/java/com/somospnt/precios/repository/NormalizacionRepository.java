/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.repository;

import com.somospnt.precios.domain.Normalizacion;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

public interface NormalizacionRepository extends CrudRepository<Normalizacion, Long> {

    List<Normalizacion> findAll(Sort sort);
    List<Normalizacion> findByDescripcionNueva(String descripcion);
    

}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.repository;

import com.somospnt.precios.domain.Producto;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto, Long> {

    @Modifying
    @Query("update Producto p set p.vigente = false")
    void actualizarParaNoVigente();

    @Query(value = "select * from producto where match(descripcion) against (?1 in boolean mode) and vigente = true order by descripcion", nativeQuery = true)
    List<Producto> findByText(String descripcion);

    List<Producto> findByDescripcionIgnoreCaseOrderByFechaAsc(String descripcion);
    Optional<Producto> findByDescripcionIgnoreCaseAndVigente(String descripcion, boolean vigente);

    @Modifying
    @Query("update Producto p set p.descripcion = ?1 where p.descripcion = ?2")
    void actualizarDescripcion(String descripcion, String descripcionOriginal);

}

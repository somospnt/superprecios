/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.domain;

public enum Origen {

    COTO("Coto"),
    DISCO("Disco"),
    JUMBO("Jumbo"),
    WALMART("Walmart"),
    CUIDADOS("Precios Cuidados")
    ;

    private String nombre;

    private Origen(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}

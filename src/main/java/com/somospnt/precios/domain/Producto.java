/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.precios.domain;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

@Entity
public class Producto {

    @Id
    @GeneratedValue
    private Long id;
    private String descripcion;
    private BigDecimal precioCuidados;
    private BigDecimal precioCoto;
    private BigDecimal precioDisco;
    private BigDecimal precioJumbo;
    private BigDecimal precioWalmart;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fecha;
    private boolean vigente;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecioCuidados() {
        return precioCuidados;
    }

    public void setPrecioCuidados(BigDecimal precioCuidados) {
        this.precioCuidados = precioCuidados;
    }

    public BigDecimal getPrecioCoto() {
        return precioCoto;
    }

    public void setPrecioCoto(BigDecimal precioCoto) {
        this.precioCoto = precioCoto;
    }

    public BigDecimal getPrecioDisco() {
        return precioDisco;
    }

    public void setPrecioDisco(BigDecimal precioDisco) {
        this.precioDisco = precioDisco;
    }

    public BigDecimal getPrecioJumbo() {
        return precioJumbo;
    }

    public void setPrecioJumbo(BigDecimal precioJumbo) {
        this.precioJumbo = precioJumbo;
    }

    public BigDecimal getPrecioWalmart() {
        return precioWalmart;
    }

    public void setPrecioWalmart(BigDecimal precioWalmart) {
        this.precioWalmart = precioWalmart;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isVigente() {
        return vigente;
    }

    public void setVigente(boolean vigente) {
        this.vigente = vigente;
    }

    public void setPrecio(BigDecimal precio, Origen origen) {
        switch (origen) {
            case CUIDADOS:
                precioCuidados = precio;
                break;
            case COTO:
                precioCoto = precio;
                break;
            case DISCO:
                precioDisco = precio;
                break;
            case JUMBO:
                precioJumbo = precio;
                break;
            case WALMART:
                precioWalmart = precio;
                break;
            default:
                throw new UnsupportedOperationException("Origen no soportado: " + origen.name());
        }
    }

    public BigDecimal getPrecio(Origen origen) {
        switch (origen) {
            case CUIDADOS:
                return precioCuidados;
            case COTO:
                return precioCoto;
            case DISCO:
                return precioDisco;
            case JUMBO:
                return precioJumbo;
            case WALMART:
                return precioWalmart;
            default:
                throw new UnsupportedOperationException("Origen no soportado: " + origen.name());
        }
    }

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", descripcion=" + descripcion + ", precioCuidados=" + precioCuidados + ", precioCoto=" + precioCoto + ", precioDisco=" + precioDisco + ", precioJumbo=" + precioJumbo + ", precioWalmart=" + precioWalmart + ", fecha=" + fecha + ", vigente=" + vigente + '}';
    }

}

package com.somospnt.precios.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Normalizacion implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String descripcionOrigen;
    private String descripcionNueva;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcionOrigen() {
        return descripcionOrigen;
    }

    public void setDescripcionOrigen(String descripcionOrigen) {
        this.descripcionOrigen = descripcionOrigen;
    }

    public String getDescripcionNueva() {
        return descripcionNueva;
    }

    public void setDescripcionNueva(String descripcionNueva) {
        this.descripcionNueva = descripcionNueva;
    }
}

-- Las columnas se usan como clave, hacer que sean case-sensitive
ALTER TABLE normalizacion CHANGE descripcion_origen descripcion_origen VARCHAR(500) COLLATE latin1_bin NOT NULL;
ALTER TABLE normalizacion CHANGE descripcion_nueva descripcion_nueva VARCHAR(500) COLLATE latin1_bin NOT NULL;
-- Cambiar campo TIMESTAMP a DATETIME. En MySQL el campo TIMESTAMP se actualiza
-- automaticamente con los updates, y no es lo que queremos.
ALTER TABLE producto CHANGE fecha fecha DATETIME NOT NULL;
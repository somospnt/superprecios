DROP TABLE IF EXISTS producto;

CREATE TABLE producto (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    descripcion VARCHAR(500) NOT NULL,
    precio_cuidados DOUBLE,
    precio_coto DOUBLE,
    precio_disco DOUBLE,
    precio_jumbo DOUBLE,
    precio_walmart DOUBLE,
    fecha TIMESTAMP NOT NULL,
    vigente BOOLEAN NOT NULL
) ENGINE = MyISAM;

CREATE FULLTEXT INDEX producto_fulltext ON producto(descripcion);
CREATE INDEX producto_descripcion ON producto(descripcion);
CREATE INDEX producto_vigente ON producto(vigente);
CREATE UNIQUE INDEX uk_producto_descripcion_fecha on producto(descripcion,fecha);

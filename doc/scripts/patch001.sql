DROP TABLE IF EXISTS normalizacion;

CREATE TABLE normalizacion (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    descripcion_origen VARCHAR(500) NOT NULL,
    descripcion_nueva VARCHAR(500) NOT NULL
);

CREATE UNIQUE INDEX uk_normalizacion_origen_descripcion on normalizacion(descripcion_origen);